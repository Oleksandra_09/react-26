import "../Contacts.scss";

import Icon from "../../Icon/Icon";

const Phones = () => {
    return(
        <div className="phones">
           <div className="phones__phone row">
                <Icon src="/images/icons/phone.png" alt="phone" height="18px"/>
                <span>
                    <a href="tel:+38050320945">+380503200945</a>
                </span>
           </div>
           <div className="phones__phone row">
                <Icon src="/images/icons/phone.png" alt="phone" height="18px"/>
                <span>
                    <a href="tel:+380505621868">+380505621868</a>
                </span>
           </div>
        </div>
    )
}

export default Phones;