import { render,screen,fireEvent } from "@testing-library/react";

import Button from "./Button";

describe("test Button", () => {
    test("isButton", () => {
        render(<Button>text</Button>)
        expect(screen.getByText("text")).toBeInTheDocument()
    })

    test("isType", () => {
        render(<Button>text</Button>)
        expect(screen.getByText("text")).toHaveAttribute("type", "button")
    })

    test("change type", () => {
        render(<Button type="submite">text</Button>)
        expect(screen.getByText("text")).toHaveAttribute("type", "submite")
    })

    test("renders with src prop", () => {
        const mockSrc = "path/to/image.jpg";
        render(<Button src={mockSrc}>Click me</Button>);

        // Проверка наличия изображения
        const imageElement = screen.getByAltText("icon");
        expect(imageElement).toBeInTheDocument();
        expect(imageElement).toHaveAttribute("src", mockSrc);

        // Проверка текста кнопки
        const buttonElement = screen.getByText("Click me");
        expect(buttonElement).toBeInTheDocument();
    });

    test("renders without src prop", () => {
        render(<Button>Click me</Button>);

        // Проверка отсутствия изображения
        const imageElement = screen.queryByAltText("icon");
        expect(imageElement).toBeNull();

        // Проверка текста кнопки
        const buttonElement = screen.getByText("Click me");
        expect(buttonElement).toBeInTheDocument();
    });

    test("calls onClick handler", () => {
        const onClickMock = jest.fn();
        render(<Button onClick={onClickMock}>text</Button>);
        const buttonElement = screen.getByText("text");
        fireEvent.click(buttonElement);
        expect(onClickMock).toHaveBeenCalled();
    });
})