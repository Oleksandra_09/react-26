import { render } from '@testing-library/react';
import { Provider } from "react-redux";
import store from "../../store";
import { BrowserRouter } from "react-router-dom";
import Footer  from "./Footer";

describe('Footer', () => {
    test('renders footer', () => {
        const footer = render(
            <Provider store={store}>
                <BrowserRouter>
                    <Footer/>
                </BrowserRouter>
            </Provider>   
          )
        expect(footer).toMatchSnapshot();
    })
})