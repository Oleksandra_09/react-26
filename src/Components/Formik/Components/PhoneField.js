import PropTypes from "prop-types";
import { useField, ErrorMessage} from 'formik';
import { PatternFormat } from 'react-number-format';
import cn from "classnames";


const PhoneField = ({ name, children }) => {
    const [field] = useField({ name, children });
    
    return (
      <>
        <div className={cn("formic-wrapper__field")}>
          <div>
            <label htmlFor={name}>
              {children}
            </label>
            <PatternFormat {...field} format="+38 (0##) ## ## ###" allowEmptyFormatting/>
          </div>
            <ErrorMessage name={name}>{msg => <div className="formic-wrapper__error">{msg}</div>}</ErrorMessage>
        </div>
      </>
    );
  };

PhoneField.propTypes = {
    name: PropTypes.string
};

export default PhoneField;