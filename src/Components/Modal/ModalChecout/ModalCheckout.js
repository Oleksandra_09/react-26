import PropTypes from "prop-types";

import ModalWrapper from "./../ModalComponents/ModalWrapper"
import Modal from "./../ModalComponents/Modal";
import ModalHeader from "./../ModalComponents/ModalHeader";
import ModalBody from "./../ModalComponents/ModalBody";
import FormikWrapper from "../../Formik/Components/FormikWrapper";

const ModalCheckout = ({isOpenModalCheckout, toggleOpenModalCheckout}) => {
    
    return (  
        <ModalWrapper isOpen={isOpenModalCheckout}>
            <Modal closeModal={()=> {toggleOpenModalCheckout()}}>
                <ModalHeader optionalClassName="modal-checkout">Оформлення замовлення</ModalHeader>
                <ModalBody optionalClassName="modal-checkout">
                    <FormikWrapper toggleOpenModalCheckout={toggleOpenModalCheckout}/>
                </ModalBody>
            </Modal>
        </ModalWrapper>
    )
}

ModalCheckout.propTypes = {
    isOpenModalCheckout: PropTypes.bool,
    toggleOpenModalCheckout: PropTypes.func,
};

export default ModalCheckout;