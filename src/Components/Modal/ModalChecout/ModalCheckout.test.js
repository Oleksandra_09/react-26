import { render, screen, fireEvent } from "@testing-library/react";
import { Provider } from "react-redux";
import store from "../../../store";

import ModalCheckout from "./ModalCheckout";

describe('ModalCheckout', () => {
    test('renders ModalCheckout and interacts with it', () => {
        const toggleOpenModalCheckoutMock = jest.fn();
        render(
            <Provider store={store}>  
                <ModalCheckout isOpenModalCheckout={true} toggleOpenModalCheckout={toggleOpenModalCheckoutMock}/>
            </Provider>    );
        expect(screen.getByText('Оформлення замовлення')).toBeInTheDocument();
        fireEvent.click(screen.getByTestId('close').querySelector('div'));
        expect(toggleOpenModalCheckoutMock).toHaveBeenCalledTimes(1);
      })
})