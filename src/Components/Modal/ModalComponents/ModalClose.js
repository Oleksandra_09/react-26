import PropTypes from 'prop-types';

import {ReactComponent as Close} from "../../Svg/close.svg";

const ModalClose = ({onClick}) => {
    
    return (
        <div className='modal__close' onClick={onClick}>
            <Close />
        </div>
    )
}

ModalClose.propTypes = {
    onClick: PropTypes.func
  };

export default ModalClose;