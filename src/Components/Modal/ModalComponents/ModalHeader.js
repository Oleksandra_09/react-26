import cn from "classnames";

const ModalHeader = ({children, optionalClassName}) => {

    return (
        <div className={cn('modal__header', optionalClassName)}>
            {children}
        </div>
    )
}

export default ModalHeader;