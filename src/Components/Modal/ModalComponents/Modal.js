import PropTypes from 'prop-types';
import cn from 'classnames';

import ModalClose from "./ModalClose";

const Modal = ({closeModal, children}) => {
    
    return (
        <div data-testid="close" className={cn('modal')} >
            <ModalClose onClick={closeModal}/> 
            {children}   
        </div>
    )
}

Modal.propTypes = {
    closeModal: PropTypes.func
  };

export default Modal;