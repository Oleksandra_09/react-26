import PropTypes from "prop-types";
import { useDispatch,useSelector } from "react-redux";
import { toggleCartProduct,toggleModal } from "../../../store/mainSlice";

import ModalWrapper from "./../ModalComponents/ModalWrapper"
import Modal from "./../ModalComponents/Modal";
import ModalHeader from "./../ModalComponents/ModalHeader";
import ModalBody from "./../ModalComponents/ModalBody";
import ModalImg from "./../ModalComponents/ModalImg";
import ModalFooter from "./../ModalComponents/ModalFooter";

const ModalText = () => {
    const dispatch = useDispatch();
    const isOpenModalText = useSelector(state => state.main.isOpenModalText);   
    const currentCard = useSelector(state => state.main.currentCard);  
    const { src, title } = currentCard;

    return (
        <ModalWrapper isOpen={isOpenModalText}>
            <Modal closeModal={() => dispatch(toggleModal("modalText"))}>
                <ModalImg src={src} alt={title}></ModalImg>
                <ModalHeader>{title}</ModalHeader>
                <ModalBody optionalClassName="body-price">Do you want to delete?</ModalBody>
                <ModalFooter 
                    firstText="Yes"
                    secondaryText="No" 
                    firstClick = {() => {
                        dispatch(toggleCartProduct({currentCard: currentCard, action: "remove"}));
                        dispatch(toggleModal("modalText"));
                    }}
                    secondaryClick = {() => dispatch(toggleModal("modalText"))}/>
            </Modal>
        </ModalWrapper>
    )
}

ModalText.propTypes = {
    isOpenModalCart: PropTypes.bool,
    toggleModalCart: PropTypes.func,
    currentCard: PropTypes.object
  };

export default ModalText;