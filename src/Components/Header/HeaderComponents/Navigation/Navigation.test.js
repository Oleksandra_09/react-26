import { render } from '@testing-library/react';
import { Provider } from "react-redux";
import store from "../../../../store";
import { BrowserRouter } from "react-router-dom";
import Navigation  from "./Navigation";

describe('Navigation', () => {
    test('renders navigation', () => {
        const navigation = render(
            <Provider store={store}>
                <BrowserRouter>
                    <Navigation></Navigation>        
                </BrowserRouter>
            </Provider>
          )
        expect(navigation).toMatchSnapshot()
    })
})
