import NavigationItem from "./NavigationItem";

const SubNavigation = () => {
    const menuList = [{name: 'bike', path: '/catalog/bike'},
    {name: 'controller', path: '/catalog/controller'},
    {name: 'accumulator', path: '/catalog/accumulator'},
    {name: 'motor', path: '/catalog/motor'},
    {name: 'accessories', path: '/catalog/accessories'},
    {name: 'elektro-kits', path: '/catalog/elektro-kits'}
]

    const menuItems = menuList.map((item, index) =>
    <NavigationItem key={index} to={item.path} name={item.name}/>
  );

    return (
        <nav className="sub-navigation">
            <ul className="sub-navigation__list">
                {menuItems}
            </ul>
        </nav>
    )
}
export default SubNavigation;