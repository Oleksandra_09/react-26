import { Link,useMatch } from 'react-router-dom';
import cn from 'classnames';

import SubNavigation from './SubNavigation';

const NavigationItem = ({to, name, subnavigation}) => {
    const match = useMatch(to);
    
    return (
        <li className={cn("navigation__item",subnavigation && "submenu")}>
            <Link to={to} className={cn(match?"active":"")}>{name}</Link>
        {subnavigation && <SubNavigation />}
        </li>
    )
}

export default NavigationItem;