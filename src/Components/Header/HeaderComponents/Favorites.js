import PropTypes from 'prop-types';
import {useSelector} from "react-redux";

import {ReactComponent as Heart} from "../../Svg/heart.svg";

const Favorites = ({/*favorites*/}) => {
    const favorites = useSelector(state => state.main.favorites);

    return (
        <div className="header__heart icon">
            <Heart />
            <span>{favorites.length}</span>
        </div>
    )
}

Favorites.propTypes = {
    //favorites: PropTypes.array
  };

export default Favorites;