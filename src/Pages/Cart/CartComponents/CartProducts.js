import {useSelector} from "react-redux";
import React, { useEffect } from 'react';

import CartProductCard from "./CartProductCard";
import ModalText from "../../../Components/Modal/ModalText/ModalText";
import Empty from "../../../Components/Empty/Empty";

const CartProducts = () => {
    const cartProducts = useSelector(state => state.main.cartProducts);

    return (
        <>
            <div className="cart__products">
                {cartProducts.length === 0 && <Empty />}
                {cartProducts.length > 0 && cartProducts.map((product)=> <CartProductCard 
                key={product.id}
                product={product}
                ></CartProductCard>)}
            </div>
            <ModalText />   
        </> 
    )  
}

export default CartProducts;