import { useContext } from "react"; 
import {useSelector} from "react-redux";

import "./Favorites.scss";

import Title from "../../Components/Title/Title";
import Products from "../../Components/Products/Products";
import { ReactComponent as DataTable} from "../../Components/Svg/data_table.svg";
import { ReactComponent as Cards} from "../../Components/Svg/cards.svg";
import { ProductsViewContext } from "../../Components/AppRoutes/AppRoutes";

const Favorites = ({toggleProductsView}) => {
    const favorites = useSelector(state => state.main.favorites);

    const productsView = useContext(ProductsViewContext);

    return (
        <div className="favorites">
            <div className="interior-content">
                <Title>Обране</Title>
                <div className="catalog__switcher"> 
                    {productsView?<DataTable onClick={toggleProductsView}/>:<Cards onClick={toggleProductsView}/>} 
                </div>
                <Products products={favorites}/>
            </div>
        </div>
    )
}

export default Favorites; 