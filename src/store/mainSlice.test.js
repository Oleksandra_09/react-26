import mainSlice,{addPoducts,setCurrentCard,toggleModal,toggleFavorites,toggleCartProduct,removeCartProducts} from './mainSlice.js';

describe("mainSlice", () => {
	test("initial state", () => {
		expect(mainSlice(undefined, {type:undefined})).toEqual({
			products: [],
      favorites: [],
      cartProducts: [],
      currentCard: {},
      isOpenModalCart: false,
      isOpenModalText: false,
		})
	})

  test("addPoducts", () => {
    const initialState = {
      products: []
    };
    const newState = mainSlice(initialState, addPoducts([{ id: 1, title: 'Product 1' }])); 
    expect(newState.products).toEqual([...[{ id: 1, title: 'Product 1' }]]);
  })

  test("setCurrentCard", () => {
    const initialState = {
      currentCard: {}
    };
    const newState = mainSlice(initialState, setCurrentCard({ id: 1, title: 'Product 1' })); 
    expect(newState.currentCard).toEqual({ id: 1, title: 'Product 1' });
  })

	test("toggleModal", () => {
    const initialState = {
        isOpenModalCart: false,
        isOpenModalText: false,
      };
    const newState = mainSlice(initialState, toggleModal("modalCart"));
    expect(newState.isOpenModalCart).toBe(true);
    expect(newState.isOpenModalText).toBe(false);
	})

  test("add to Favorites", () => {
    const initialState = {
      favorites: [{ id: 1, title: 'Product 1' }]
    };
    const newState = mainSlice(initialState, toggleFavorites({ id: 2, title: 'Product 2' })); 
    expect(newState.favorites).toEqual([{ id: 1, title: 'Product 1' },{ id: 2, title: 'Product 2' }]);
  })

  test("remove from Favorites", () => {
    const initialState = {
      favorites: [{ id: 1, title: 'Product 1' }]
    };
    const newState = mainSlice(initialState, toggleFavorites({ id: 1, title: 'Product 1' })); 
    expect(newState.favorites).toEqual([]);
  })
})

